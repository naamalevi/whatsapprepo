#include <iostream>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <cstring>
#include <vector>
#include <unistd.h>
#include "whatsappio.h"

// function declarations:
int connectToServer(char* argv[]);
void runProgram(int clientSocket, std::string clientName);
void readFromServer(int clientSocket, std::string clientName);
void readFromStdin(int clientSocket, std::string clientName);
void writeToServer(int clientSocket, std::string clientName, std::string command);
bool isValidCreateGroup(std::string clientName, std::string groupName,
                        std::vector<std::string> clients);
bool containsTwoMembers(std::string clientName, std::vector<std::string> &clients);
bool isValidName(std::string &name);
bool isValidSend(std::string clientName, std::string receiverName, std::string message);

/**
 * The main function.
 * Runs the whatsappClient that communicates with other clients using the server, according
 * to its protocol.
 * @param argc - the number of the program's arguments.
 * @param argv - array of the arguments, expects to three argument:
 * clientName - the client's user name.
 * serverAddress - the ip address of the computer that runs the server.
 * serverPort - an unsigned int represents the communication port.
 * @return - 0 in success, 1 otherwise.
 */
int main(int argc, char * argv[]) {
    if (argc != 4)
    {
        print_client_usage();
        exit(1);
    }
    std::string clientName = argv[1];
    if (!isValidName(clientName))
    {
        print_client_usage();
        exit(1);
    }
    int clientSocket = connectToServer(argv);
    writeToServer(clientSocket, clientName, ""); //notify the server the new client's name
    // - according to the protocol
    runProgram(clientSocket, clientName);
}

/**
 * Connects to the server and returns the fd of the new connection socket.
 * @param argv - an array of the program's arguments.
 * @return the fd of the new connection socket.
 */
int connectToServer(char* argv[])
{
    int clientSocket;
    struct sockaddr_in sa;
    memset(&sa, 0, sizeof(struct sockaddr_in));
    sa.sin_family = AF_INET;
    int portNum;
    try
    {
        portNum = std::stoi(argv[3], 0, 10);
    }
    catch(const std::invalid_argument &e)
    {
        print_error("stoi", errno);
        exit(1);
    }
    catch (const std::out_of_range &e)
    {
        print_error("stoi", errno);
        exit(1);
    }
    sa.sin_port = htons(portNum);
    inet_aton(argv[2], &(sa.sin_addr));

    if ((clientSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        print_error("socket", errno);
        exit(1);
    }
    if (connect(clientSocket, (struct sockaddr *)&sa, sizeof(sa)) < 0)
    {
        print_fail_connection();
        print_error("connect", errno);
        close(clientSocket);
        exit(1);
    }
    print_connection();
    return clientSocket;
}

/**
 * Runs all the program activity.
 * If the server send it a message - read it and act accordingly.
 * If the user typed a new command - check if valid, if so ask the server to execute it.
 * @param clientSocket - the fd of the connection socket.
 * @param clientName - the name of the client.
 */
void runProgram(int clientSocket, std::string clientName)
{
    fd_set readfds;
    while (true)
    {
        FD_ZERO(&readfds);
        FD_SET(clientSocket, &readfds);
        FD_SET(STDIN_FILENO, &readfds);
        if (select(clientSocket+1, &readfds, NULL, NULL, NULL) < 0)
        {
            print_error("select", errno);
            writeToServer(clientSocket, clientName, "client_fail");
            close(clientSocket);
            exit(1);
        }
        if (FD_ISSET(clientSocket, &readfds))
        {
            readFromServer(clientSocket, clientName);
        }
        if (FD_ISSET(STDIN_FILENO, &readfds))
        {
            readFromStdin(clientSocket, clientName);
        }
    }
}

/**
 * Reads a message from the server and prints it to the STDOUT.
 * @param clientSocket - the fd of the connection socket.
 * @param clientName - the name of the client.
 */
void readFromServer(int clientSocket, std::string clientName)
{
    size_t bytesToRead = WA_MAX_OUTPUT;
    size_t totalReadCtr = 0;
    ssize_t currentReadCtr = 0;
    char longMessage[WA_MAX_OUTPUT];
    char * messagePtr = longMessage;
    while (totalReadCtr < bytesToRead)
    {
        currentReadCtr = read(clientSocket, messagePtr, bytesToRead-totalReadCtr);
        if (currentReadCtr > 0)
        {
            totalReadCtr += currentReadCtr;
            messagePtr += currentReadCtr;
        }
        if (currentReadCtr < 0)
        {
            print_error("read", errno);
            writeToServer(clientSocket, clientName, "client_fail");
            close(clientSocket);
            exit(1);
        }
        if ((currentReadCtr == 0) || (longMessage[totalReadCtr-2] == '\n'))
        {
            break;
        }
    }
    std::string messageStr = longMessage;
    messageStr = messageStr.substr(0, messageStr.size()-1);
    std::cout << messageStr << std::endl;
    if (messageStr == "Unregistered successfully.")
    {
        if (close(clientSocket) == -1)
        {
            print_error("close", errno);
            writeToServer(clientSocket, clientName, "client_fail");
            close(clientSocket);
            exit(1);
        }
        exit(0);
    }
    if ((messageStr == "Client name is already in use.") || (messageStr == "Server EXIT."))
    {
        writeToServer(clientSocket, clientName, "client_fail");
        close(clientSocket);
        exit(1);
    }
}

/**
 * Reads a command from the the STDIN.
 * If the command is valid from the client's perspective - sends the server an ask to execute it,
 * otherwise prints an error message.
 * @param clientSocket - the fd of the connection socket.
 * @param clientName - the name of the client.
 */
void readFromStdin(int clientSocket, std::string clientName)
{
    std::string command;
    std::getline(std::cin, command);
    if (std::cin.goodbit)
    {
        print_error("getline", errno);
        writeToServer(clientSocket, clientName, "client_fail");
        close(clientSocket);
        exit(1);
    }
    command_type commandType;
    std::string name;
    std::string message;
    std::vector<std::string> clients;
    parse_command(command, commandType, name, message, clients);
    switch (commandType)
    {
        case INVALID_CREATE_GROUP:
            print_create_group(false, false, clientName, name);
            return;
        case INVALID_SEND:
            print_send(false, false, clientName, name, message);
            return;
        case INVALID:
            print_invalid_input();
            return;
        case CLIENT_FAIL:
            print_invalid_input();
            return;
        case CREATE_GROUP:
            if(!isValidCreateGroup(clientName, name, clients))
            {
                return;
            }
            break;
        case SEND:
            if(!isValidSend(clientName, name, message))
            {
                return;
            }
            break;
        default:
            break;
    }
    writeToServer(clientSocket, clientName, " " + command);
}

/**
 * Write a given command to the server, according to the protocol.
 * @param clientSocket - the fd of the connection socket.
 * @param clientName - the name of the client.
 * @param command - a command that the client ask to execute.
 */
void writeToServer(int clientSocket, std::string clientName, std::string command)
{
    std::string toWrite = clientName + command + "\n";
    const char * message = toWrite.c_str();
    size_t bytesToWrite = toWrite.length() +1 ;
    size_t totalWriteCtr = 0;
    ssize_t currentWriteCtr = 0;

    while (totalWriteCtr < bytesToWrite) {
        currentWriteCtr = write(clientSocket, message, bytesToWrite - totalWriteCtr);
        if (currentWriteCtr > 0) {
            totalWriteCtr += currentWriteCtr;
            toWrite += currentWriteCtr;
        }
        if (currentWriteCtr < 0) {
            print_error("write", errno);
            writeToServer(clientSocket, clientName, "client_fail");
            close(clientSocket);
            exit(1);
        }
        if (currentWriteCtr == 0) //got to end of file
        {
            break;
        }
    }
}

/**
 * Returns true if the create_group command is valid - if the new group's name is valid, 
 * and if the new group contains at least two different members.
 * @param clientName a string representing the name of the client that creates the group.
 * @param groupName a string representing the new group's name.
 * @param clients a std::vector<std::string> containing the new group's members.
 * @return true if the create_group command is valid, false otherwise.
 */
bool isValidCreateGroup(std::string clientName, std::string groupName, 
std::vector<std::string> clients)
{
    if((!isValidName(groupName)) || (!containsTwoMembers(clientName, clients)))
    {
        print_create_group(false, false, clientName, groupName);
        return false;
    }
    return true;
}

/**
 * Checks whether the clients vector contains a name that is different from the given clientName.
 * @param clientName - the name of the client.
 * @param clients - a vector of names of clients, members.
 * @return whether the clients vector contains a name that is different from the given clientName
 * (true) or not (false).
 */
bool containsTwoMembers(std::string clientName, std::vector<std::string> &clients)
{
    for (unsigned int i = 0; i < clients.size(); i++)
    {
        if (clientName != clients[i])
        {
            return true;
        }
    }
    return false;
}

/**
 * Returns true if the given name is valid - contains only letters and digits.
 * @param name a reference to the name.
 * @return true if the name is valid, false otherwise.
 */
bool isValidName(std::string &name)
{
    for (unsigned int i = 0; i< name.size(); i++)
    {
        if ((!isalpha(name[i]) && (!isdigit(name[i]))))
        {
            return false;
        }
    }
    return true;
}

/**
 * Returns true if the send command is valid - if the receiver's name is different than the
 sender's name.
 * @param clientName a string representing the name of the client who sends the message.
 * @param receiverName a string representing the name of the client that the message is send to.
 * @param message the message that is send.
 * @return True if the sent is valid, false otherwise.
 */
bool isValidSend(std::string clientName, std::string receiverName, std::string message)
{
    if (clientName == receiverName)
    {
        print_send(false, false, clientName, receiverName, message);
        return false;
    }
    return true;
}
