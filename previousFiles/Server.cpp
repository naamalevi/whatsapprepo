#include <asm/param.h>
#include <netinet/in.h>
#include <unistd.h>
#include <netdb.h>
#include <cstring>
#include <cstdlib>
#include <utility>
#include <iostream>
#include <algorithm>
#include <iterator>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "whatsappio.h"

/*
 * A struct represents a group.
 * Contains all the relevant information about a group in the whatsappServer program: the group's
 * name, it's number of members and the names of it's members.
 */
struct whatsappGroup {
    std::string groupName;
    std::vector<std::string> members;
    int numOfMembers;
};

// function declarations:
int establish(unsigned short portNum, struct sockaddr_in & sa);
void initializeClients(std::pair<std::string, int>  * clientsArray);
void runProgram(int serverSocket, std::pair<std::string, int> clientsArray[60],
                std::vector<whatsappGroup> groupsVec, struct sockaddr_in &sa);
void handleStdinInput(int serverSocket, std::pair<std::string, int> * clientsArray);
void createNewConnection(int &numOfConnectedClients, int serverSocket, sockaddr_in &sa,
                         std::pair<std::string, int> *clientsArray,
                         std::vector<whatsappGroup> groupsVec);
void handleClientsCommand(int fd, std::pair<std::string, int> * clientsArray,
                          std::vector<whatsappGroup>& groupsVec, int &numOfConnectedClients);
void addSocketToClientsArray(int newSocket, std::pair<std::string, int>* clientsArray,
                             std::vector<whatsappGroup> &groupsVec, int &numOfConnectedClients);
void createNewGroup(std::string groupName, std::vector<std::string> groupMembers,
                    std::string clientName, std::vector<whatsappGroup> &groupsVec);
void createGroupFeedback(int fd, bool success, std::string clientName, std::string groupName);
bool isLogicCreateGroup(std::string groupName, std::vector<whatsappGroup> &groupVec,
                        std::vector<std::string> groupMembers,
                        std::pair<std::string, int> *clientsArray);
void sendMessage(std::string clientName, std::string receiverName, std::string message,
                 std::pair<std::string, int> *clientsArray, std::vector<whatsappGroup> &groupsVec,
                 bool isGroup);
void sendMessageToSingleClient(std::string clientName, std::string receiverName,
                               std::string message, std::pair<std::string, int> *clientsArray,
                               std::vector<whatsappGroup> &groupsVec);
bool isLogicSend(std::string clientName, std::string receiverName,
                 std::pair<std::string, int>  *clientsArray, std::vector<whatsappGroup> &groupsVec,
                 bool & isGroup);
void performWho(int fd, std::pair<std::string, int> *clientsArray);
void performExit(int fd, std::string clientName, std::pair<std::string, int> *clientsArray,
                 std::vector<whatsappGroup> &groupsVec, int &numOfConnectedClients);
void sendFeedback(int fd, bool success);
std::string readData(int socketFd);
void writeToClient(int connectionSocket, std::string command);


/**
 * The main function.
 * Runs the whatsappServer that enabled communication between different clients using
 * the server's protocol.
 * @param argc - the number of the program's arguments.
 * @param argv - array of the arguments, expects to one argument:
 * numPort - an unsigned int represents the program's port.
 * @return - 0 in success, 1 otherwise.
 */
int main(int argc, char * argv[])
{
    if (argc != 2)
    {
        print_server_usage();
        exit(1);
    }
    errno = 0;
    unsigned short portNum = (unsigned short) strtoul(argv[1], NULL,0);
    if ((errno == EINVAL) || (errno == ERANGE))
    {
        print_error("strtoul", errno);
    }
    struct sockaddr_in sa;
    int serverSocket = establish(portNum, sa);
    std::pair<std::string, int>  clientsArray[MAX_CLIENTS];
    initializeClients(clientsArray);
    std::vector<whatsappGroup> groupsVec;
    runProgram(serverSocket, clientsArray, groupsVec, sa);
    return 0;
}

/**
 * Establish the server, operates the "three steps" -
 * Creates a socket that will listen for connections, give it an address to listen to, and
 * recommend the maximum number of request that will be queued before requests start being
 * denied, to 10.
 * @param portNum - unsigned int represents the number of the program's port.
 * @param sa - a reference to struct sockaddr_in
 * @return - an int represents the file descriptor of the new socket that was created.
 */
int establish(unsigned short portNum, struct sockaddr_in & sa)
{
    char myName[MAXHOSTNAMELEN + 1];
    int serverSocket;
    struct hostent *hp;

    // hostnet initialization
    if (gethostname(myName, MAXHOSTNAMELEN) == -1)
    {
        print_error("gethostname", h_errno);
    }
    hp = gethostbyname(myName);
    if (hp == NULL)
    {
        print_error("gethostbyname", errno);

    }
    // sockaddr_in initialization
    memset(&sa, 0, sizeof(struct sockaddr_in));
    sa.sin_family = hp->h_addrtype;
    sa.sin_addr.s_addr = INADDR_ANY;
    sa.sin_port = htons(portNum);

    // create socket
    if ((serverSocket = socket(AF_INET, SOCK_STREAM, 0)) < 0)
    {
        print_error("socket", errno);
    }
    if (bind(serverSocket, (struct sockaddr*)&sa, sizeof(sa)) < 0)
    {
        close(serverSocket);
        print_error("bind", errno);
    }
    if (listen(serverSocket, 10) == -1)
    {
        print_error("listen", errno);
    }
    return serverSocket;
}

/**
 * Initializes the second entry of all the pairs in the array that is pointed by the
 * clientsArray argument to be -1.
 * @param clientsArray - a pointer to a pair of (name, fd) represents an array of all the connected
 * clients (the second entry equal to -1 indicates 'not is used' client).
 */
void initializeClients(std::pair<std::string, int>  * clientsArray)
{
    for (int i = 0 ; i <MAX_CLIENTS; i++)
    {
        clientsArray[i].second = -1;
    }
}

/**
 * Runs all the program activity.
 * If existed client send the server a command - execute it, if legal.
 * If new client try to connect - add it, if legal.
 * If the user typed in the server STDIN - if the command is EXIT, finish processing.
 * @param serverSocket - fd of the server's socket.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients.
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param sa - a reference to struct sockaddr_in.
 */
void runProgram(int serverSocket, std::pair<std::string, int> clientsArray[60],
                std::vector<whatsappGroup> groupsVec, struct sockaddr_in &sa)
{
    fd_set readfds;
    int i, maxFd, fd;
    int numOfConnectedClients = 0;

    while (true) {
        // create the actual readfds
        FD_ZERO(&readfds);
        FD_SET(STDIN_FILENO, &readfds);
        FD_SET(serverSocket, &readfds);
        maxFd = serverSocket;
        for (i = 0; i < MAX_CLIENTS; i++) {
            fd = clientsArray[i].second;
            if (fd > 0) {
                FD_SET(fd, &readfds);
            }
            if (fd > maxFd) {
                maxFd = fd;
            }
        }
        if (select(maxFd + 1, &readfds, NULL, NULL, NULL) < 0)
        {
            print_error("select", errno);
        }

        // stdin input
        if (FD_ISSET(STDIN_FILENO, &readfds))
        {
            handleStdinInput(serverSocket, clientsArray);
        }

        // a new client tries to connect
        if (FD_ISSET(serverSocket, &readfds))
        {
            createNewConnection(numOfConnectedClients, serverSocket, sa, clientsArray, groupsVec);
        }

        for (i = 0; i < MAX_CLIENTS; i++)
        {
            fd = clientsArray[i].second;
            // handle a new client's command
            if (FD_ISSET(fd, &readfds))
            {
                handleClientsCommand(fd, clientsArray, groupsVec, numOfConnectedClients);
            }
        }
    }
}

/**
 * Reads the command the was typed in the STDIN, if it EXIT - finish processing.
 * @param serverSocket - fd of the server's socket.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 */
void handleStdinInput(int serverSocket, std::pair<std::string, int> *clientsArray)
{
    std::string command;
    std::getline(std::cin, command);
    if (std::cin.goodbit)
    {
        print_error("getline", errno);
    }
    if (command == "EXIT")
    {
        for (int i = 0; i < MAX_CLIENTS; i++)
        {
            if (clientsArray[i].second != -1)
            {
                writeToClient(clientsArray[i].second, "Server EXIT.\n");
                if (close(clientsArray[i].second) == -1)
                {
                    print_error("close", errno);
                }
            }
        }
        print_exit();
        if (close(serverSocket) == -1)
        {
            print_error("close", errno);
        }
        exit(0);
    }
}

/**
 * Creates a new socket for the new connected client and add it to the clientsArray.
 * If the user name is not valid, prints a message and closes the socket.
 * @param numOfConnectedClients - a reference to int represents the number of connected clients.
 * @param serverSocket -  fd of the server's socket.
 * @param sa - a reference to struct sockaddr_in.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 */
void createNewConnection(int &numOfConnectedClients, int serverSocket, sockaddr_in &sa,
                         std::pair<std::string, int> *clientsArray,
                         std::vector<whatsappGroup> groupsVec)
{
    int newSocket;
    if (numOfConnectedClients < MAX_CLIENTS)
    {
        int addrlen = sizeof(sa);
        if ((newSocket = accept(serverSocket, (struct sockaddr *)&sa, (socklen_t *)&addrlen)) < 0) {
            print_error("accept", errno);
        }
        addSocketToClientsArray(newSocket, clientsArray, groupsVec, numOfConnectedClients);
    }
}

/**
 * Reads a command from connected client and execute it if the command is valid.
 * @param fd - the fd of the connection socket.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param numOfConnectedClients - a reference to int represents the number of connected clients.
 */
void handleClientsCommand(int fd, std::pair<std::string, int> * clientsArray,
                          std::vector<whatsappGroup>& groupsVec, int &numOfConnectedClients)
{
    std::string connectionMessage = readData(fd);
    unsigned long firstSpace = connectionMessage.find(' ');
    std::string command = connectionMessage.substr(firstSpace + 1);
    std::string clientName = connectionMessage.substr(0, firstSpace);
    command_type commandType;
    std::string name;
    std::string message;
    std::vector<std::string> clients;
    parse_command(command, commandType, name, message, clients);
    bool success = false;
    bool isGroup = false;
    switch (commandType)
    {
        case CREATE_GROUP:
            if(isLogicCreateGroup(name, groupsVec, clients, clientsArray))
            {
                success = true;
                createNewGroup(name, clients, clientName, groupsVec);
            }
            print_create_group(true, success, clientName, name);
            createGroupFeedback(fd, success, clientName, name);
            break;
        case SEND:
            if(isLogicSend(clientName, name, clientsArray, groupsVec, isGroup))
            {
                success = true;
                sendMessage(clientName, name, message, clientsArray, groupsVec, isGroup);
            }
            print_send(true, success, clientName, name, message);
            sendFeedback(fd, success);
            break;
        case WHO:
            performWho(fd, clientsArray);
            print_who_server(clientName);
            break;
        case EXIT:
            writeToClient(fd, "Unregistered successfully.\n");
            performExit(fd, clientName, clientsArray, groupsVec, numOfConnectedClients);
            print_exit(true, clientName);
            break;
        case CLIENT_FAIL:
            performExit(fd, clientName, clientsArray, groupsVec, numOfConnectedClients);
            break;
    }
}

/**
 * Adds a new socket to the clientsArray.
 * If the user name is not valid, prints a message and closes the socket.
 * @param newSocket - the fd of the new created connection socket.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param numOfConnectedClients - a reference to int represents the number of connected clients.
 */
void addSocketToClientsArray(int newSocket, std::pair<std::string, int>* clientsArray,
                             std::vector<whatsappGroup> &groupsVec, int &numOfConnectedClients)
{
    int i;
    std::string name = readData(newSocket);
    bool isUnique = true;
    // check if the name is used as a group name:
    for (i = 0; i < groupsVec.size(); i++)
    {
        if (groupsVec[i].groupName == name)
        {
            isUnique = false;
        }
    }
    // check if the name is used as a client's name
    for (i = 0; i < MAX_CLIENTS; i++)
    {
        if ((clientsArray[i].first == name) && (clientsArray[i].second != -1))
        {
            isUnique = false;
        }
    }
    if (!isUnique)
    {
        writeToClient(newSocket,"Client name is already in use.\n");
        print_dup_connection();
        if (close(newSocket) == -1)
        {
            print_error("close", errno);
        }
        return;
    }
    for (i = 0; i <MAX_CLIENTS; i++)
    {
        if (clientsArray[i].second == -1)
        {
            clientsArray[i].first = name;
            clientsArray[i].second = newSocket;
            break;
        }
    }
    numOfConnectedClients++;
    print_connection_server(name);
}

/**
 * Creates a new group according to the client's command.
 * @param groupName - a string represents the name of the new group.
 * @param groupMembers - a vector of strings, vector of names of the new group's members.
 * @param clientName - the name of the client that ask to create the new group.
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 */
void createNewGroup(std::string groupName, std::vector<std::string> groupMembers,
                    std::string clientName, std::vector<whatsappGroup> &groupsVec)
{
    try
    {
        groupMembers.push_back(clientName);
    }
    catch (const std::bad_alloc &)
    {
        print_error("push_back", errno);
    }

    std::vector<std::string> members;
    int numOfMembers = 0;
    for (int i = 0 ; i < groupMembers.size(); i++){
        if (std::find(std::begin(members), std::end(members), groupMembers[i]) == std::end(members))
        {
            try
            {
                members.push_back(groupMembers[i]);
            }
            catch (const std::bad_alloc &)
            {
                print_error("push_back", errno);
            }
            numOfMembers++;
        }
    }
    whatsappGroup newGroup = {groupName, members, numOfMembers};
    try
    {
        groupsVec.push_back(newGroup);
    }
    catch (const std::bad_alloc &)
    {
        print_error("push_back", errno);
    }
}

/**
 * Write the client the feedback for its create_group command, whether it was created
 * successfully or not.
 * @param fd - the fd of the connection socket.
 * @param success - a boolean that indicates whether the group was created successfully or not.
 * @param clientName - the name of the client that ask to create the new group.
 * @param groupName - a string represents the name of the new group.
 */
void createGroupFeedback(int fd, bool success, std::string clientName, std::string groupName)
{
    std::string feedBack;
    if(success) {
        feedBack = "Group \"" + groupName + "\" was created successfully.\n";
    } else {
        feedBack = "ERROR: failed to create group \"" + groupName + "\".\n";
    }
    writeToClient(fd, feedBack);
}

/**
 * Checks whether the group that the client ask to create is valid from the server's perspective:
 * its name is not in use and all the members are connected.
 * @param groupName - a string represents the name of the new group.
 * @param groupVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param groupMembers - a vector of strings, vector of names of the new group's members.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @return a boolean indicates whether the desired group is logic (true), or not (false).
 */
bool isLogicCreateGroup(std::string groupName, std::vector<whatsappGroup> &groupVec,
                        std::vector<std::string> groupMembers,
                        std::pair<std::string, int> *clientsArray)
{
    int i,j;
    //check if no user with the same name
    for (i = 0; i < MAX_CLIENTS; i++) {
        if ((clientsArray[i].first == groupName) && (clientsArray[i].second != -1)) {
            return false;
        }
    }

    // check if group name is already in use
    for (i = 0; i < groupVec.size(); i++) {
        if (groupVec[i].groupName == groupName) {
            return false;
        }
    }

    // check if all group members are connected
    for (i = 0; i < groupMembers.size(); i++)
    {
        for (j = 0; j < MAX_CLIENTS; j++)
        {
            if ((clientsArray[j].first == groupMembers[i]) && (clientsArray[i].second != -1))
            {
                break;
            }
        }
        if (j == MAX_CLIENTS)
        {
            return false;
        }
    }
}

/**
 * Send a message to a specific client or group, according to client's command.
 * @param clientName - the name of the client that ask to send a message.
 * @param receiverName - the name of the client/group that should receive the message.
 * @param message - a string that the client want to send.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param isGroup - a boolean that indicates whether the receiver is a group or a single client.
 */
void sendMessage(std::string clientName, std::string receiverName, std::string message,
                 std::pair<std::string, int> *clientsArray, std::vector<whatsappGroup> &groupsVec,
                 bool isGroup)
{
    if (!isGroup)
    {
        sendMessageToSingleClient(clientName, receiverName, message, clientsArray, groupsVec);

    }
    else {
        for (int i = 0; i < groupsVec.size(); i++)
        {
            if (groupsVec[i].groupName == receiverName)
            {
                for (int j = 0; j < groupsVec[i].numOfMembers; j++)
                {
                    if (groupsVec[i].members[j] != clientName){
                        sendMessageToSingleClient(clientName, groupsVec[i].members[j], message,
                                                  clientsArray, groupsVec);
                    }
                }
                return;
            }
        }
    }
}

/**
 * Send a given message to a given client.
 * @param clientName - the name of the client that ask to send a message.
 * @param receiverName - the name of the client that should receive the message.
 * @param message - a string that the client want to send.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 */
void sendMessageToSingleClient(std::string clientName, std::string receiverName,
                               std::string message, std::pair<std::string, int> *clientsArray,
                               std::vector<whatsappGroup> &groupsVec)
{
    int receiverFd = 0;
    for (int i = 0; i < MAX_CLIENTS; i++)
    {
        if (clientsArray[i].first == receiverName)
        {
            receiverFd = clientsArray[i].second;
        }
    }
    std::string messageToSend = clientName + ": " + message + "\n";
    writeToClient(receiverFd, messageToSend);
}

/**
 * Checks whether the send command that the client ask is valid from the server's perspective:
 * the receiver is a name of exist group or client.
 * Also change the isGroup to true if the receiverName is name of a group.
 * @param clientName - the name of the client that asked to send a message.
 * @param receiverName - the name of the client/group that should receive the message.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param isGroup - a boolean that indicates whether the receiver is a group or a single client,
 * is change to contain the correct value during the run of the function.
 * @return - a boolean indicates whether the desired send command is logic (true), or not (false).
 */
bool isLogicSend(std::string clientName, std::string receiverName,
                 std::pair<std::string, int>  *clientsArray, std::vector<whatsappGroup> &groupsVec,
                 bool & isGroup)
{
    for(int i = 0; i< groupsVec.size(); i++)
    {
        if (groupsVec[i].groupName == receiverName)
        {
            isGroup = true;
            if (std::find(std::begin(groupsVec[i].members), std::end(groupsVec[i].members),
                          clientName)
                != std::end(groupsVec[i].members))
            {
                return true;
            }
        }
    }
    for (int i = 0; i < MAX_CLIENTS; i++) {
        if ((clientsArray[i].first == receiverName) && (clientsArray[i].second != -1)) {
            return true;
        }
    }
    return false;
}

/**
 * Writes the client a list of currently connected client's names (alphabetically order),
 * separated by comma without spaces.
 * @param fd - the fd connection socket.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 */
void performWho(int fd, std::pair<std::string, int> *clientsArray)
{
    std::string allClientsStr;
    std::vector<std::string> connectedClientsNames;
    bool isFirst = true;
    for (int i =0; i < MAX_CLIENTS; i++)
    {
        if (clientsArray[i].second != -1)
        {
            try
            {
                connectedClientsNames.push_back(clientsArray[i].first);
            }
            catch (const std::bad_alloc &)
            {
                print_error("push_back", errno);
            }
        }
    }
    sort(connectedClientsNames.begin(), connectedClientsNames.end());
    for (int i = 0; i < connectedClientsNames.size(); i++)
    {
        if (isFirst)
        {
            allClientsStr = connectedClientsNames[i];
            isFirst = false;
        } else
        {
            allClientsStr = allClientsStr + "," + connectedClientsNames[i];
        }
    }
    writeToClient(fd, allClientsStr+"\n");
}

/**
 * Stop connect with a client that typed the exit command.
 * @param fd - the fd connection socket.
 * @param clientName - the name of the client that asked to exit.
 * @param clientsArray - array size 60 of pairs of (name, fd) represents all the connected clients,
 * (as a pointer)
 * @param groupsVec - a vector of whatsappGroup represents all the group that exists in the program.
 * @param numOfConnectedClients - a reference to int represents the number of connected clients.
 */
void performExit(int fd, std::string clientName, std::pair<std::string, int> *clientsArray,
                 std::vector<whatsappGroup> &groupsVec, int &numOfConnectedClients)
{
    // remove from all groups
    for (int i = 0; i < groupsVec.size(); i++) {
        for (int j = 0; j < groupsVec[i].members.size(); j++) {
            if (groupsVec[i].members[j] == clientName) {
                groupsVec[i].members.erase(std::remove(groupsVec[i].members.begin(),
                                                       groupsVec[i].members.end(), clientName),
                                           groupsVec[i].members.end());
                groupsVec[i].numOfMembers--;
                break;
            }
        }

    }
    numOfConnectedClients--;
    // remove from clientsArray
    for (int i = 0; i < MAX_CLIENTS; i++){
        if (clientsArray[i].first == clientName){
            clientsArray[i].second = -1;
        }
    }
    if (close(fd) == -1)
    {
        print_error("close", errno);
    }
}

/**
 * Writes the client the feedback for its send command, whether it was sent successfully or not.
 * @param fd - the fd of the connection socket.
 * @param success - a boolean that indicates whether the message was sent successfully or not.
 */
void sendFeedback(int fd, bool success)
{
    std::string feedBack;
    if(success) {
        feedBack = "Sent successfully.\n";
    } else {
        feedBack = "ERROR: failed to send.\n";
    }
    writeToClient(fd, feedBack);
}

/**
 * Reads a message from a given client, and returns it.
 * @param socketFd - the fd of the connection socket.
 * @return - the readen message.
 */
std::string readData(int socketFd)
{
    char data[WA_MAX_CONNECTION_INPUT];
    char * dataPtr = data;
    size_t totalReadCtr = 0;
    ssize_t currentReadCtr = 0;

    while (totalReadCtr < WA_MAX_CONNECTION_INPUT) {
        currentReadCtr = read(socketFd, dataPtr, WA_MAX_CONNECTION_INPUT - totalReadCtr);
        if (currentReadCtr > 0) {
            totalReadCtr += currentReadCtr;
            dataPtr += currentReadCtr;

        }
        if (currentReadCtr < 0) {
            print_error("read", errno);
        }
        if ((currentReadCtr == 0) ||(data[totalReadCtr-2] == '\n'))
        {
            break;
        }
    }
    std::string dataStr = data;
    dataStr = dataStr.substr(0, dataStr.size()-1);
    return dataStr;

}

/**
 * Writes the given message to the given client, according to the protocol rules.
 * @param connectionSocket - the fd of the connection socket.
 * @param command - a message to write to the client.
 */
void writeToClient(int connectionSocket, std::string command)
{
    const char * message = command.c_str();
    size_t bytesToWrite = command.length() +1 ;
    size_t totalWriteCtr = 0;
    ssize_t currentWriteCtr = 0;

    while (totalWriteCtr < bytesToWrite) {
        currentWriteCtr = write(connectionSocket, message, bytesToWrite - totalWriteCtr);
        if (currentWriteCtr > 0) {
            totalWriteCtr += currentWriteCtr;
            command += currentWriteCtr;
        }
        if (currentWriteCtr < 0) {
            print_error("write", errno);
        }
        if (currentWriteCtr == 0) //got to end of file
        {
            break;
        }
    }
}