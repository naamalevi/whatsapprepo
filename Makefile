CC = g++
CFLAGS = -std=c++11

CLIENT_FILES = whatsappClient.cpp whatsappio.cpp
SERVER_FILES = whatsappServer.cpp whatsappio.cpp
HEADER_FILES = whatsappio.h

CXXFLAGS = -Wall -std=c++11 -g
ALL_FILES = $(CLIENT_FILES) $(SERVER_FILES) $(HEADER_FILES) README Makefile

all: client server

client: $(CLIENT_FILES) $(HEADER_FILES)
	$(CC) $(CXXFLAGS) $(CLIENT_FILES) -o whatsappClient
	
server: $(SERVER_FILES) $(HEADER_FILES)
	$(CC) $(CXXFLAGS) $(SERVER_FILES) -o whatsappServer
	
clean:
	rm -rf *.o
	
tar: $(ALL_FILES)
	tar -cvf ex4.tar $(ALL_FILES)
